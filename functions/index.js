const functions = require('firebase-functions');
const requestLib = require('request');
const rp = require('request-promise');

//const http = require('http');
const dns = require('dns')

// const {Firestore} = require('@google-cloud/firestore');

//const claraProxyIp = `http://windyplace.ddns.net`;
const claraProxyHost =  `windyplace.ddns.net`;
//const claraProxyHost = `75.64.219.110`;
const claraProxyPort = `80`;

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions.https.onRequest((request, response) => {

dns.lookup(claraProxyHost,(err, address, family) => {
    if (err) {
        console.log(err);
        response.send(`${err}`);
        return;
    }
    console.log(`Home address is ${address}`);
    response.send(`${address}`);
});

 //response.send("Hello from Firebase!  We're ready to make our proxy.");
});

exports.api = functions.https.onRequest((request, response) => {

    if(request.method === "OPTIONS"){
        response.setHeader(`Access-Control-Allow-Origin`, `*`)
        response.setHeader(`Access-Control-Allow-Methods`, `GET, POST, PUT`)
        response.setHeader(`Access-Control-Allow-Headers`, `Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With`)
        response.setHeader(`Content-type`, `text/plain`)
        
        response.send("Cors Options Reponse");
        
    }else{

        let responseBody = "";

        console.log('hostname: ' + claraProxyHost);
        console.log('port: ' + claraProxyPort);
        console.log('path: ' + `/api${request.path}`);
        console.log('method: ' + request.method);

        dns.lookup(claraProxyHost,(err, proxyAddress, family) => {
            
            if(err){
                response.send(err);
                console.log(err);
                return;
            }
            
            console.log(`Home address is ${proxyAddress}`);

            let fullPath = `http://${proxyAddress}/api${request.path}`
            console.log(`${request.method} requst for ${fullPath}`);

            let options = {
                method: request.method, 
                headers: request.headers,
                resolveWithFullResponse: true
            };

            if(request.method === "POST"){
                let stringBody = JSON.stringify(request.body);
                options.body = stringBody
                console.log(stringBody);
            }

            rp(fullPath, options).then( proxyResponse => {

                //console.log(`numheaders ${proxyResponse.headers}`)

                response.headers = proxyResponse.headers;

                response.setHeader(`Access-Control-Allow-Origin`, `*`)
                response.setHeader(`Access-Control-Allow-Methods`, `GET, POST, PUT`)
                response.setHeader(`Access-Control-Allow-Headers`, `Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With`)
                // response.setHeader(`Content-type`, `text/plain`)

                response.send(proxyResponse.body);    
                return; 

            }).catch(err=>{
                response.send(err);
                console.log(err);
            });      
        })
    }    
});