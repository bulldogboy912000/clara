module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        'ftp-deploy': {
            build: {
              auth: {
                host: '192.168.1.117',
                port: 22,
                authKey: 'key1'
              },
              src: './dist/clara',
              dest: '/var/www/html',
              exclusions: ['path/to/source/folder/**/.DS_Store', 'path/to/source/folder/**/Thumbs.db', 'path/to/dist/tmp']
            }
          }
    });
  
    // // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-ftp-deploy');
  
    // Default task(s).
    grunt.registerTask('default', ['ftp-deploy']);
  
  };