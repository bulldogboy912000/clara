#https://stackoverflow.com/questions/51893854/intercept-and-filter-requests-to-an-https-server-in-python

from twisted.internet import reactor, ssl
from twisted.web import proxy, server
from twisted.web.resource import Resource
from twisted.web.static import File

import firebase_admin
from firebase_admin import credentials, auth, firestore

import OpenSSL.crypto

from os import path

basepath = path.dirname(__file__)
#certpath = path.abspath(path.join(basepath, "cupcakes", "server.pem"))
firebasepath = path.abspath(path.join(basepath, "cupcakes", "clara-39c27-firebase-adminsdk-c1mog-9d10c61742.json"))


#For Auth... f
#https://firebase.google.com/docs/admin/setup/
#https://firebase.google.com/docs/auth/admin/verify-id-tokens

firebaseCreds = credentials.Certificate(firebasepath)
firebase_admin.initialize_app(firebaseCreds)
db = firestore.client()

class CorsPreflightResponse(Resource):
    isLeaf = True
    def render(self, request):
        print("boop")
        return b'Response'

corsPreflightResponse = CorsPreflightResponse()
#corsPreflightResponse.putChild(b'api', CorsPreflightResponse())
#corsPreflightResponse.putChild(b'api/door', CorsPreflightResponse())


class LocalResource(Resource):
    def render(self, request):
        return "Success"


def userHasRoles(uid, requestedRoles):
    user_ref = db.collection(u'users').document(u"{}".format(uid))
    user_data = user_ref.get().to_dict()
    if('roles' not in user_data):
        return False
        
    roles = user_data['roles']
    for requestedRole in requestedRoles:
        #print(requestedRole)
        if requestedRole not in roles:
            return False
        if roles[requestedRole] == False:
            return False
    return True

    #docs = users_ref.get()
    # for doc in docs:
    #     print(u'{} => {}'.format(doc.id, doc.to_dict()))

class FirebaseAuthReverseProxyResource(Resource, object):

    def getChild(self, path, request):
        #TODO: Looging

        request.setHeader(b'Access-Control-Allow-Origin', b'*')
        request.setHeader(b'Access-Control-Allow-Methods', b'GET, PUT, POST')
        request.setHeader(b'Access-Control-Allow-Headers', b'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With')
        request.setHeader(b'Content-type', b'text/plain')

        #print(request.method)
        if(request.method == b'OPTIONS'):
            return corsPreflightResponse

        authenticated = False

        id_token = request.getHeader('Authorization')

        if(id_token and not id_token.startswith("Basic")):
            decoded_token = auth.verify_id_token(id_token)
            uid = decoded_token['uid']
            
            if(uid and userHasRoles(uid, ['trusted'])):
                return proxy.ReverseProxyResource('192.168.1.114', 1880, b"/" + path)
  
        return File.forbidden

if __name__ == "__main__":

    #with open(certpath) as keyAndCert:
        #certificate = ssl.PrivateCertificate.loadPEM(keyAndCert.read())

    site = server.Site(FirebaseAuthReverseProxyResource())
    #reactor.listenSSL(6969, site, certificate.options())
    reactor.listenTCP(6969, site)
    print("Start")
    reactor.run()
