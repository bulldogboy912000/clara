
## Objective
The goal of this project is to create an implementation to show examples of my work using best practices and common tools.

## Toolbelt
* Angular
* CI/CD
* State Management
* Unit Testing
* Git

## Tracing a Request

### Firebase Hosting
Request starts out from the client side.  These requests are either fetching the status of the door (status updates via polling) or setting the desired status of the door (open or closed).

### Firebase Functions
CORS Proxy between the user and my home network.  

### ReverseProxy.py
Reverse Proxy and authentication-check/roles-check with Google Firebase

### Voyage
Node Red webserver that distributes requests across IoT Devices
![Node Red Flow](/documentation/node_red_flow.png)


### Jaguar (Endpoint)
ESP8266 microcontroller to detect and manipulate my garage door

## Known Issues
* Lingering state after logging out and back in
* Login page should redirect to home if the user is already logged in

## Brainstorming

* Garage Door

* Security

* Lighting

* Logging

* Development server


### Theme
* UI: Material
* Primary: Indego (A800: #536dfe)

### Icon
* logomakr.com/8Ad2UQ



## Building

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
