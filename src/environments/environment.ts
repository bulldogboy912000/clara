// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAeM7h9CxitG-fxEF6qz-6RIgkjaJHKGQc",
    authDomain: "clara-39c27.firebaseapp.com",
    databaseURL: "https://clara-39c27.firebaseio.com",
    projectId: "clara-39c27",
    storageBucket: "clara-39c27.appspot.com",
    messagingSenderId: "370454058835",
    appId: "1:370454058835:web:08109e8d2fe764ee"
  },
  voyageProxy: `https://us-central1-clara-39c27.cloudfunctions.net`
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
