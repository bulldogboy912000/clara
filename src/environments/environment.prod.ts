export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyAeM7h9CxitG-fxEF6qz-6RIgkjaJHKGQc",
    authDomain: "clara-39c27.firebaseapp.com",
    databaseURL: "https://clara-39c27.firebaseio.com",
    projectId: "clara-39c27",
    storageBucket: "clara-39c27.appspot.com",
    messagingSenderId: "370454058835",
    appId: "1:370454058835:web:08109e8d2fe764ee"
  },
  voyageProxy: `https://us-central1-clara-39c27.cloudfunctions.net`
};
