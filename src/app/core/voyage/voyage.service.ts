import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map, startWith, switchMap, catchError } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { Subject, of, Observable, interval, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';

export enum HttpMethod{
  get = "GET",
  post = "POST",
  put = "PUT",
}


export enum Status{
  open = "Open",
  moving = "Moving",
  closed = "Closed",
  unknown = "Unknown",
}

export interface VoyageInterface{

  status$: Subject<Status>;
  setDoor(status: Status): Observable<any>;
  getDoorStatus(): Observable<Status>;

}

@Injectable({
  providedIn: 'root'
})
export class VoyageService implements VoyageInterface{

  public root: string = environment.voyageProxy;

  public status$: Subject<Status>;

  public poll$: Subscription;

  constructor(
    private httpClient: HttpClient,
    private afAuth: AngularFireAuth,
  ) {
    this.status$ = new Subject<Status>();
    this.status$.next(Status.unknown);

    this.poll$ = interval(5000).pipe(
      startWith(0),
      switchMap((value, index)=>{
        return this.getDoorStatus();
      })
    ).subscribe(status=>this.status$.next(status));

  }

  public setDoor(status: Status): Observable<any>{
    if(![Status.open, Status.closed].includes(status)){
      throw(`Invalid status: ${status}`);
    }

    let wantOpen = 0;
    if(status === Status.open){
      wantOpen = 1;
    }
    
    return this.makeCall(`${this.root}/api/door/open`, HttpMethod.post, `{"wantOpen": ${wantOpen}}`).pipe(
      map((results:any) => {
        this.status$.next(
          this.adaptDoorStatusFromRaw(results)
        );

      })
    );
  }

  public getDoorStatus(): Observable<Status> {
    return this.makeCall(`${this.root}/api/door/open`).pipe(
      map(this.adaptDoorStatusFromRaw)
    );
  }

  public makeCall( path: string, method: HttpMethod = HttpMethod.get, options: string = ""){

    let user = this.afAuth.auth.currentUser;

    if(!user){
      console.warn("User Not Authorized");
      return of(Status.unknown);
      ;
    }

    let token = JSON.parse(JSON.stringify(user)).stsTokenManager.accessToken;

    let headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET',
      'Content-Type': 'application/json',
      'Authorization': token
    });

    let returnObservable: Observable<any>;

    if(method === HttpMethod.get){
      returnObservable = this.httpClient.get(path, {headers: headers});
    }

    if(method === HttpMethod.post){
      returnObservable = this.httpClient.post(path, options, {headers: headers});
    }

    if(method === HttpMethod.put){
      returnObservable = this.httpClient.put(path, options, {headers: headers});
    }

    return returnObservable.pipe(
      catchError((err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', err.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error(`Backend returned code ${err.status}, body was: ${err.error}`);
        }

        // ...optionally return a default fallback value so app can continue (pick one)
        // which could be a default value
        // return Observable.of<any>({my: "default value..."});
        // or simply an empty observable
        return of(null);
      })
    )
   
  }

  public adaptDoorStatusFromRaw(raw: string){
    let newStatus = Status.unknown;
    if(raw == "0"){
      newStatus = Status.closed;
    }
    if(raw == "1"){
      newStatus = Status.open;
    }
    return newStatus;
  }
}

export class MockVoyageService implements VoyageInterface{
  public status$ = new Subject<Status>();
  private status: Status = Status.closed;

  getDoorStatus(){
    return this.status$;
  }
  setDoor(){
    if(this.status === Status.closed){
      this.status = Status.open;
    }else{
      this.status = Status.closed;
    }

    this.status$.next(this.status);

    return of(this.status);
  }
}