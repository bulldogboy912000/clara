import { TestBed } from '@angular/core/testing';

import { VoyageService, HttpMethod, Status } from './voyage.service';

import {
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment.prod';
import { NgxsModule } from '@ngxs/store';
import { JaguarState } from 'src/app/modules/jaguar/state/jaguar.state';
import { of } from 'rxjs';

describe('VoyageService', () => {

  let service: VoyageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NgxsModule.forRoot([JaguarState]),
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
      ]
    });
    service =  TestBed.get(VoyageService);
  }
  );

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set the door status', () => {
    let storeSpy = spyOn(service, 'makeCall').and.returnValue(of(1));
    //let callSpy = spyOn(service, "setDoor").and.returnValue(of(Status.open))
    let result = service.setDoor(Status.open);
    //result.subscribe(status => expect(status).toBe(Status.open));
    expect(storeSpy).toHaveBeenCalled();
  });

  it('should throw an error on invalid door status', () => {
    expect(_ => service.setDoor(Status.unknown)).toThrow('Invalid status: Unknown');
  });

  it('should adapt door status from raw', () => {
    expect(service.adaptDoorStatusFromRaw("0")).toEqual(Status.closed);
    expect(service.adaptDoorStatusFromRaw("1")).toEqual(Status.open);
  });

  //https://books.google.com/books?id=cOlODwAAQBAJ&pg=PA163&lpg=PA163&dq=angular+firebase+auth+stub+user+testing&source=bl&ots=msNSQHq0no&sig=ACfU3U25YJef1vlpcQtTtOlk_FN4tt53sg&hl=en&sa=X&ved=2ahUKEwj675zDsPHiAhXIct8KHUEpCjcQ6AEwB3oECAgQAQ#v=onepage&q=angular%20firebase%20auth%20stub%20user%20testing&f=false
  
  // it('should make calls to the backend', () => {
  //   service.afAuth.auth.currentUser = new User('id', 'email', 'display', 'photourl', []);
  //   let getSpy = spyOn(service.httpClient, 'get').and.returnValue({});
  //   service.makeCall('path', HttpMethod.get);
  //   expect(getSpy).toHaveBeenCalled();
  // });

  // it('should set the door status', () => {
  //   let storeSpy = spyOn(service.store, 'dispatch').and.returnValue({});

  //   // [Status.open, Status.closed].forEach(status => {

  //   // });
  //   service.setDoor(Status.open);
  //   expect(storeSpy).toHaveBeenCalled();

  // });
});
