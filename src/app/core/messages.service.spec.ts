import { TestBed } from '@angular/core/testing';

import { MessagesService } from './messages.service';

describe('MessagesService', () => {

  let service: MessagesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service =  TestBed.get(MessagesService);
  });

  it('should be created', () => {
    
    expect(service).toBeTruthy();
  });

  it('should push a new message', () => {
    let testMessage = "Hello World";
    service.messages.subscribe(message => expect(message).toBe(testMessage));
    service.pushMessage(testMessage);
  });
});
