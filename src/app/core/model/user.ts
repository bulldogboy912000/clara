export enum Roles {
    trusted = "trusted",
    admin = "admin",
}

export class User{
    constructor(
        public uid: string,
        public email: string,
        public displayName: string,
        public photoURL: string,
        public roles: object,
    ){};
}

export class UserAdapter{
    static fromRaw(raw: any): User{
        return new User(
            raw.uid,
            raw.email,
            raw.displayName,
            raw.photoURL,
            raw.roles,
        );
    }
}