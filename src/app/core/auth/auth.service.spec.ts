import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment.prod';
import { RouterTestingModule } from '@angular/router/testing';
import { auth } from 'firebase';

describe('AuthService', () => {

    let service: AuthService;

    beforeEach(() => {TestBed.configureTestingModule({
      imports: [
        AngularFireModule.initializeApp(environment.firebase), 
        AngularFireAuthModule, 
        AngularFirestoreModule,
        RouterTestingModule,
      ]
    });
    service = TestBed.get(AuthService);
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should signOut', ()=>{
    let signoutSpy = spyOn(service.afAuth.auth, 'signOut').and.returnValue(
      new Promise((resolve, reject)=>resolve())
    );

    //let navSpy = spyOn(service.router, 'navigate').and.stub();

    service.signOut();

    expect(signoutSpy).toHaveBeenCalled();
    //expect(navSpy).toHaveBeenCalled();
  });

  it('should do googleLogin', ()=>{
    spyOn(service, 'oAuthLogin').and.returnValue(true);
    expect(service.googleLogin()).toBe(true);
  });

  it('should do oAuthLogin', ()=>{
    let popupSpy = spyOn(service.afAuth.auth, 'signInWithPopup').and.returnValue(new Promise((resolve, reject)=>{resolve();}));
    service.oAuthLogin(new auth.EmailAuthProvider());
    expect(popupSpy).toHaveBeenCalled();
  });
});
