import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  public messages: Subject<string> = new Subject<string>();

  constructor() { }

  pushMessage(message:string){
    this.messages.next(message);
  }
}
