import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { MatSnackBarModule } from '@angular/material';
import { User } from '../model/user';

describe('UserService', () => {

  let service: UserService;

  beforeEach(() => {TestBed.configureTestingModule({
      imports: [ 
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        MatSnackBarModule,
      ]
    
    });
    service = TestBed.get(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get users', () => {
    let collectionSpy = spyOn(service.afs, 'collection').and.returnValue(
      {
        valueChanges: function(){
          return {pipe:function(){}}
        }
      }
    );;
    service.getUsers();
    expect(collectionSpy).toHaveBeenCalled();
  });

  it('should massage the users data, which sounds absolutely disgusting and is potentially illegal', ()=>{
    let result = service.massageUsersData([
      
        {"name": "Test 1"},
        {"name": "Test 2", "roles": []},
        {"name": "Test 3", "roles": [{"admin": false}]},
        {"name": "Test 4", "roles": [{"trusted": false}]},
        {"name": "Test 5", "roles": [{"admin": false}, {"trusted": false}]},
    ]);

    result.forEach(user => {
      expect(user.roles.admin).toBe(false);
      expect(user.roles.trusted).toBe(false);
    })
  });

  it('should set a user trust level to true', ()=>{
    let docSpy = spyOn(service.afs, 'doc').and.returnValue({set: function(data, merge){

    }});
    service.setUserTrust(new User("uid", "email", "displayname", "photourl", {trusted: false}), true);
    expect(docSpy).toHaveBeenCalled();

  });

});
