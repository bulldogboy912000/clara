import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ModulesModule } from './modules/modules.module';
import { NgxsModule } from '@ngxs/store';
import { JaguarState } from './modules/jaguar/state/jaguar.state';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment.prod';
import { CoreModule } from './core/core.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { BrowserModule } from '@angular/platform-browser';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        NoopAnimationsModule,
        RouterTestingModule,
        NgxsModule.forRoot([JaguarState]),
        //NgxsReduxDevtoolsPluginModule.forRoot(),
        AngularFireModule.initializeApp(environment.firebase),
        CoreModule, // <-- add core module
        ModulesModule,   
        LayoutModule,
      ],
      declarations: [
        AppComponent,
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
