import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';
import { LoginComponent } from './modules/login/login.component';
import { JaguarComponent } from './modules/jaguar/jaguar.component';
import { HomeComponent } from './modules/home/home.component';
import { UserListRealComponent } from './modules/user-list-real/user-list-real.component';


const routes: Routes = [
  ///...
  { path: 'door', component: JaguarComponent,  canActivate: [AuthGuard] },
  { path: 'users', component: UserListRealComponent,  canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent},
  { path: '', component: HomeComponent,  canActivate: [AuthGuard] },
  { path: '**', component: HomeComponent,  canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
