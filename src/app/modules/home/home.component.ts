import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/model/user';
import { AuthService } from 'src/app/core/auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  public user$: Observable<User> = this.authService.user;
  constructor(
    //private breakpointObserver: BreakpointObserver,
    public authService: AuthService,
  ) {}
  
}
