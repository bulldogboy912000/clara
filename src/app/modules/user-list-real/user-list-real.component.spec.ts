import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserListRealComponent } from './user-list-real.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';

describe('UserListRealComponent', () => {
  let component: UserListRealComponent;
  let fixture: ComponentFixture<UserListRealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFirestoreModule,
        SharedModule,
      ],
      declarations: [ UserListRealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserListRealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
