import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/core/user/user.service';

@Component({
  selector: 'app-user-list-real',
  templateUrl: './user-list-real.component.html',
  styleUrls: ['./user-list-real.component.scss']
})
export class UserListRealComponent implements OnInit {

  public users$: Observable<any>;

  constructor(
    public userService: UserService,
  ) { 
    this.users$ = this.userService.getUsers();
  }

  ngOnInit() {}

  setUserTrust(event){
    this.userService.setUserTrust(event.user, event.trust);
  }
}
