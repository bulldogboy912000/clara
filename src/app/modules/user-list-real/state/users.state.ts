import { State, Selector } from '@ngxs/store';

export interface UsersStateModel {
    items: string[];
}

@State<UsersStateModel>({
    name: 'users',
    defaults: {
        items: []
    }
})
export class UsersState {

    @Selector()
    public static getState(state: UsersStateModel) {
        return state;
    }

}
