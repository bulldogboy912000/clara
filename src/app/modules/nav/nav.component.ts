import { Component, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth/auth.service';
import { User } from 'src/app/core/model/user';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {

  @ViewChild('drawer') sidenav; 

  public sidenavOpen: boolean = false;

  public user$: Observable<User> = this.authService.user;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private afA: AngularFireAuth,
    private router: Router,
    private authService: AuthService,
    
  ) {}
  logout(){
    this.afA.auth.signOut().then(() => {
       this.router.navigate(['login']);  
       this.sidenavOpen = false;
    });
  }

  closeSidenav(){
    this.sidenavOpen = false;
  }

  openSidenav(){
    this.sidenavOpen = true;
  }
}
