import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { JaguarComponent } from './jaguar/jaguar.component';
import { CoreModule } from '../core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { NavComponent } from './nav/nav.component';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatGridListModule, MatCardModule, MatMenuModule, MatSnackBarModule, MatSlideToggleModule, MatChipsModule, MatProgressSpinnerModule } from '@angular/material';
import { HomeComponent } from './home/home.component';
import { LayoutModule } from '@angular/cdk/layout';
import { AppRoutingModule } from '../app-routing.module';

import { SharedModule } from '../shared/shared.module';
import { UserListRealComponent } from './user-list-real/user-list-real.component';

@NgModule({
  declarations: [
    LoginComponent,
    JaguarComponent,
    NavComponent,
    HomeComponent,
    UserListRealComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AppRoutingModule,
    CoreModule,
    HttpClientModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    MatChipsModule,
    MatProgressSpinnerModule,
    LayoutModule,
    SharedModule,
  ],
  exports: [
    LoginComponent,
    JaguarComponent,
    NavComponent,
  ]
})
export class ModulesModule { }
