import { State, Action, StateContext, Store, Select } from '@ngxs/store';
import { JaguarUpdateStatusAction, JaguarToggleDoorAction, JaguarRefreshAction } from './jaguar.actions';
import { VoyageService, Status } from 'src/app/core/voyage/voyage.service';
import { interval, Observable } from 'rxjs';
import { startWith, switchMap, tap } from 'rxjs/operators';

export class JaguarStateModel {
  public status: Status;
  public statusText: string;
}

@State<JaguarStateModel>({
  name: 'jaguar',
  defaults: {
    status: Status.unknown,
    statusText: "Unknown (Initial)"
  }
})

export class JaguarState {

  constructor(
    public store: Store,
    public voyageService: VoyageService,
  ){
    this.voyageService.status$.subscribe(newStatus => this.store.dispatch(new JaguarUpdateStatusAction(newStatus)));
  }

  @Action(JaguarRefreshAction)
  refresh(ctx: StateContext<JaguarStateModel>, action: JaguarRefreshAction){
    this.voyageService.getDoorStatus().subscribe(
      status => this.store.dispatch(new JaguarUpdateStatusAction(status))
    );
  }

  @Action(JaguarToggleDoorAction)
  toggle(ctx: StateContext<JaguarStateModel>, action: JaguarUpdateStatusAction){
    if(ctx.getState().status === Status.open){
      this.voyageService.setDoor(Status.closed).subscribe();
    }
    if(ctx.getState().status === Status.closed){
      this.voyageService.setDoor(Status.open).subscribe();
    }

    this.store.dispatch(new JaguarUpdateStatusAction(Status.unknown));

  }

  @Action(JaguarUpdateStatusAction)
  update(ctx: StateContext<JaguarStateModel>, action: JaguarUpdateStatusAction) {
    if(ctx.getState().status != action.status){

      let status = action.status;
      let statusText = "Unknown";

      //do something with user state
      if(status.toString() === Status.open){
        statusText = "Open";
      }
      if(status.toString() === Status.closed){
        statusText = "Closed";
      }
      if(status.toString() === Status.moving){
        statusText = "Moving"
      }

      ctx.patchState({
        status: action.status,
        statusText: statusText,
      });
    }
  }
}
