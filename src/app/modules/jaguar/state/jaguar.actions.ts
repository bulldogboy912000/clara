import { Status } from "src/app/core/voyage/voyage.service";


export class JaguarRefreshAction {
  static readonly type = '[Jaguar] RefreshStatus';
  constructor() { }
}

export class JaguarUpdateStatusAction {
  static readonly type = '[Jaguar] UpdateState';
  constructor(public status: Status) { }
}

export class JaguarToggleDoorAction {
  static readonly type = '[Jaguar] ToggleDoor';
  constructor() { }
}

