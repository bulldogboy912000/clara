import { TestBed, async } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { JaguarState } from './jaguar.state';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment.prod';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { VoyageService, MockVoyageService } from 'src/app/core/voyage/voyage.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('Jaguar state', () => {
  let store: Store;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        //HttpClientTestingModule,
        //AngularFireModule.initializeApp(environment.firebase), 
        //AngularFirestoreModule,
        //AngularFireAuthModule,
        NgxsModule.forRoot([JaguarState])
      ],
      providers: [
        { provide: VoyageService, useClass: MockVoyageService},
      ],
      //schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents();
    store = TestBed.get(Store);
  }));

  it('should compile', () => {
    expect(store).toBeTruthy();
  });
  // Without Unit Testing support from NGXS, this seems to be difficult versus the time I have
  // it('should create an action and add an item', () => {
  //   store.dispatch(new JaguarUpdateStatusAction(Status.unknown));
  //   store.select(state => state.jaguar.items).subscribe((items: string[]) => {
  //     expect(items).toEqual(jasmine.objectContaining([ 'item-1' ]));
  //   });
  // });

});
