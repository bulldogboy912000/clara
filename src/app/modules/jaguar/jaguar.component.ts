import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { JaguarState } from './state/jaguar.state';
import { Observable, interval, of, from } from 'rxjs';
import { JaguarToggleDoorAction, JaguarRefreshAction } from './state/jaguar.actions';
import { Status } from 'src/app/core/voyage/voyage.service';
import { startWith, tap, delay } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-jaguar',
  templateUrl: './jaguar.component.html',
  styleUrls: ['./jaguar.component.scss']
})
export class JaguarComponent implements OnInit {

  @Select(JaguarState) status$: Observable<Status>;
  public statusText: String = "";
  public status: Status = Status.unknown;
  public disabled: boolean = false;

  constructor(
    public store: Store,
    public _snackBar: MatSnackBar,
  ) {
    //I'm sure I can use pipes instead of this
    this.store.select(state => state.jaguar).subscribe((val: any) => {
      this.statusText = val.statusText;
      this.status = val.status;
    });

    this.store.dispatch(new JaguarRefreshAction());
  }

  ngOnInit() {

  }

  toggleDoor() {

    if(!([Status.open, Status.closed].includes(this.status))){
      this._snackBar.open("Unable to send a request", null, {
        duration: 3000
      })
    }else{
    this.store.dispatch(new JaguarToggleDoorAction());
    }

    this.disabled = true;
    of(false).pipe(
      delay(15 * 1000),
    ).subscribe(disabled => {
      this.disabled = disabled
    })

    //  of("").pipe(
    //    tap(_ => this.disabled = true),
    //    delay(15*1000),
    //    tap(_ => this.disabled = false)
    //  ).subscribe();

  }

}
