import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JaguarComponent } from './jaguar.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from 'src/environments/environment.prod';
import { VoyageService, MockVoyageService, Status } from 'src/app/core/voyage/voyage.service';
import { Subject } from 'rxjs';
import { MatProgressSpinnerModule, MatButtonModule, MatSnackBarModule } from '@angular/material';
import { NgxsModule } from '@ngxs/store';
import { JaguarState } from './state/jaguar.state';
import { JaguarToggleDoorAction } from './state/jaguar.actions';

describe('JaguarComponent', () => {
  let component: JaguarComponent;
  let fixture: ComponentFixture<JaguarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NgxsModule.forRoot([JaguarState]),
        AngularFireModule.initializeApp(environment.firebase), 
        AngularFirestoreModule,
        AngularFireAuthModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        MatSnackBarModule,
        RouterTestingModule,
        
      ],
      declarations: [ JaguarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JaguarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', ()=>{
    expect(component).toBeTruthy();
  })
  it('toggleDoor should toggle throw an error on unknown door status', () => {
    component.status = Status.unknown;
    let mySpy = spyOn(component._snackBar, 'open').and.stub();
    
    component.toggleDoor();
    expect(mySpy).toHaveBeenCalled();
  });
  it('toggleDoor should toggle the door', () => {
    component.status = Status.open;
    let mySpy = spyOn(component.store, 'dispatch').and.stub();
    
    component.toggleDoor();
    expect(mySpy).toHaveBeenCalledWith(new JaguarToggleDoorAction());
  });
});
