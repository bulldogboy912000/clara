import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserListComponent } from './user-list/user-list.component';
import { MatButtonModule, MatListModule, MatCardModule, MatChipsModule } from '@angular/material';


@NgModule({
  declarations: [UserListComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatListModule,
    MatCardModule,
    MatChipsModule,
  ],
  exports: [UserListComponent]
})
export class SharedModule { }
