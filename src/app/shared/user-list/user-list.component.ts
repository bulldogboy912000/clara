import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/core/model/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  @Input() public users: User[];
  @Output() public trustChange: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}
  
  ngOnInit() {}

  setUserTrust(event: Event, user: User, trust: boolean){
    this.trustChange.emit({user: user, trust: trust});
  }
}
